#!/usr/bin/env python

###############################################################################
#  Copyright 2016 Sasanka Nagavalli
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import math

import rospy
from geometry_msgs.msg import Twist, Vector3
from turtlebot_myo.msg import Gesture

STATES = ['stopped','forward','left','right']
SPEEDS = ['slow','medium','fast']

GESTURE_TO_STATE = {
  Gesture.GESTURE_FIST : STATES.index('stopped'),
  Gesture.GESTURE_FINGERS_SPREAD : STATES.index('forward'),
  Gesture.GESTURE_WAVE_IN : STATES.index('left'),
  Gesture.GESTURE_WAVE_OUT : STATES.index('right'),
}

state = 0
speed = 0

def gesture_callback(message):
    global state, speed
    if message.gesture == Gesture.GESTURE_DOUBLE_TAP:
        speed = (speed+1) % len(SPEEDS)
    elif message.gesture in GESTURE_TO_STATE: 
        state = GESTURE_TO_STATE[message.gesture]


def turtlebot_motion():
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
    rospy.init_node('select_motion', anonymous=True)
    rospy.Subscriber('gesture', Gesture, gesture_callback)

    rate = rospy.Rate(10) 
    while not rospy.is_shutdown():
        rospy.loginfo("State: " + STATES[state] + ", Speed: " + SPEEDS[speed])
        
        if STATES[state] == 'stopped':   v, w = 0, 0
        elif STATES[state] == 'forward': v, w = 0.075, 0
        elif STATES[state] == 'left':    v, w = 0, math.pi/8.0
        elif STATES[state] == 'right':   v, w = 0, -math.pi/8.0
        
        if SPEEDS[speed] == 'slow':     multiplier = 1
        elif SPEEDS[speed] == 'medium': multiplier = 2
        elif SPEEDS[speed] == 'fast':   multiplier = 4
        
        twist = Twist(Vector3(0,0,0), Vector3(0,0,0))
        twist.linear.x = v * multiplier
        twist.angular.z = w * multiplier
        
        pub.publish(twist)
        rate.sleep()


if __name__ == '__main__':
    try:
        turtlebot_motion()
    except rospy.ROSInterruptException:
        pass

