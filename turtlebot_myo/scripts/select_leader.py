#!/usr/bin/env python

###############################################################################
#  Copyright 2016 Sasanka Nagavalli
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import math

import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist, Vector3
from turtlebot_myo.msg import Gesture

MODES = ['selection', 'control']
ROBOTS = ['']

mode = 0
selected = 0
twist = Twist(Vector3(0,0,0), Vector3(0,0,0))
leader_pub = rospy.Publisher('leader', String, queue_size=1)
cmd_pubs = {}
led_pubs = {}

def gesture_callback(message):
    global mode, selected, twist
    if message.gesture == Gesture.GESTURE_DOUBLE_TAP:
        mode = (mode+1) % len(MODES)
        twist.linear.x, twist.angular.z = 0.0, 0.0
        if MODES[mode] == 'selection':
            leader_pub.publish(String(''))
        elif MODES[mode] == 'control':
            leader_pub.publish(String(ROBOTS[selected]))
    elif message.gesture == Gesture.GESTURE_WAVE_IN:
        if MODES[mode] == 'selection': 
            selected = (selected+len(ROBOTS)-1) % len(ROBOTS)
        elif MODES[mode] == 'control': 
            twist.linear.x, twist.angular.z = 0.0, math.pi/8.0
    elif message.gesture == Gesture.GESTURE_WAVE_OUT:
        if MODES[mode] == 'selection':
            selected = (selected+1) % len(ROBOTS) 
        elif MODES[mode] == 'control':
            twist.linear.x, twist.angular.z = 0.0, -math.pi/8.0
    elif message.gesture == Gesture.GESTURE_FINGERS_SPREAD:
        if MODES[mode] == 'control':
            twist.linear.x, twist.angular.z = 0.15, 0.0
    elif message.gesture == Gesture.GESTURE_FIST:
        if MODES[mode] == 'control':
            twist.linear.x, twist.angular.z = 0.0, 0.0
    rospy.loginfo("mode: " + MODES[mode] + ", selected: " + ROBOTS[selected])
        
def select_leader():
    rospy.init_node('select_leader', anonymous=True)

    robot_ids = rospy.get_param('~robot_ids')
    if len(robot_ids) > 0:
        ROBOTS[:] = robot_ids.keys()
    rospy.loginfo('Found: ' + ' '.join(ROBOTS))

    for name in ROBOTS: 
        led_topic = name + '/teleop/css_color'
        led_pubs[name] = rospy.Publisher(led_topic, String, queue_size=1)
        cmd_topic = name + '/teleop/cmd_vel'
        cmd_pubs[name] = rospy.Publisher(cmd_topic, Twist, queue_size=1)

    rospy.Subscriber('gesture', Gesture, gesture_callback)

    rospy.loginfo("mode: " + MODES[mode] + ", selected: " + ROBOTS[selected])
    rate = rospy.Rate(10) 
    while not rospy.is_shutdown():
        default_color = 'red'
        selected_color = 'green' if MODES[mode]=='control' else 'blue'
        for name in ROBOTS:
            color = selected_color if name==ROBOTS[selected] else default_color
            led_pubs[name].publish(String(color))
        if MODES[mode] == 'control': 
            cmd_pubs[ROBOTS[selected]].publish(twist)
        rate.sleep()

if __name__ == '__main__':
    try:
        select_leader()
    except rospy.ROSInterruptException:
        pass

