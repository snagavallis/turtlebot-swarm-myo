#!/usr/bin/env python

###############################################################################
#  Copyright 2016 Sasanka Nagavalli
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import json
import socket

import rospy
from geometry_msgs.msg import Quaternion
from turtlebot_myo.msg import Gesture, EmgData, Orientation

pub_emg = rospy.Publisher('emg_data', EmgData, queue_size=1)
pub_gesture = rospy.Publisher('gesture', Gesture, queue_size=1)
pub_orientation = rospy.Publisher('orientation', Orientation, queue_size=1)

GESTURE_IDS = {
    'double_tap' : Gesture.GESTURE_DOUBLE_TAP,
    'fist' : Gesture.GESTURE_FIST,
    'wave_in' : Gesture.GESTURE_WAVE_IN,
    'wave_out' : Gesture.GESTURE_WAVE_OUT,
    'fingers_spread' : Gesture.GESTURE_FINGERS_SPREAD
}


def process_emg_data(message):
    # rospy.loginfo("EmgData: " + ' '.join(str(d) for d in message['data']))
    emg = EmgData()
    emg.myo = message['myo']
    emg.timestamp = message['timestamp']
    emg.data = message['data']
    pub_emg.publish(emg)


def process_gesture(message):
    if message['gesture'] not in GESTURE_IDS: return
    rospy.loginfo("Gesture: " + message['gesture'])
    gesture = Gesture()
    gesture.myo = message['myo']
    gesture.timestamp = message['timestamp']
    gesture.gesture = GESTURE_IDS[message['gesture']]
    pub_gesture.publish(gesture)


def process_orientation(message):
    # rospy.loginfo("Orientation: " + ' '.join(str(d) for d in message['quaternion_xyzw']))
    q = Quaternion()
    q.x, q.y, q.z, q.w = tuple(message['quaternion_xyzw'])
    orientation = Orientation()
    orientation.myo = message['myo']
    orientation.timestamp = message['timestamp']
    orientation.orientation = q
    pub_orientation.publish(orientation)    


MESSAGE_TYPES = {
    'emg_data' : {
        'handler' : process_emg_data,
        'expected_keys' : ['myo', 'timestamp', 'data'],
    },
    'gesture' : {
        'handler' : process_gesture,
        'expected_keys' : ['myo', 'timestamp', 'gesture'],
    },
    'orientation' : {
        'handler' : process_orientation,
        'expected_keys' : ['myo', 'timestamp', 'quaternion_xyzw'],
    },
}


def myo_receiver():
    rospy.init_node('myo_receiver', anonymous=True)   
    
    udp_host = rospy.get_param('~host', '192.168.1.100')
    udp_port = rospy.get_param('~port', 59050)
    udp_data_size = rospy.get_param('~data_size', 16384)

    rospy.loginfo('Starting UDP server on ' + udp_host + ':' + str(udp_port))
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((udp_host, udp_port))
    rospy.loginfo('Started UDP server')
    
    while not rospy.is_shutdown():
        data, addr = sock.recvfrom(udp_data_size)
        parsed_json = json.loads(data)
        for key in parsed_json:
            if key not in MESSAGE_TYPES: continue
            message = parsed_json[key]
            if all(k in message for k in MESSAGE_TYPES[key]['expected_keys']): 
                MESSAGE_TYPES[key]['handler'](message)


if __name__ == '__main__':
    try:
        myo_receiver()
    except rospy.ROSInterruptException:
        pass

