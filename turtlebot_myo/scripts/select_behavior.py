#!/usr/bin/env python

###############################################################################
#  Copyright 2016 Sasanka Nagavalli
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import rospy
from std_msgs.msg import String
from turtlebot_myo.msg import Gesture

BEHAVIORS = ['']

selected = 0
behavior_pub = rospy.Publisher('behavior', String, queue_size=1)

def gesture_callback(message):
    global selected
    if message.gesture == Gesture.GESTURE_FIST:
        behavior_pub.publish(String(''))
    elif message.gesture == Gesture.GESTURE_WAVE_IN: 
        selected = (selected+len(BEHAVIORS)-1) % len(BEHAVIORS)
    elif message.gesture == Gesture.GESTURE_WAVE_OUT: 
        selected = (selected+1) % len(BEHAVIORS) 
    elif message.gesture == Gesture.GESTURE_FINGERS_SPREAD:
        behavior_pub.publish(String(BEHAVIORS[selected]))
    rospy.loginfo("selected behavior: " + BEHAVIORS[selected])
        
def select_behavior():
    rospy.init_node('select_behavior', anonymous=True)

    behaviors = rospy.get_param('~behaviors')
    if len(behaviors) > 0:
        BEHAVIORS[:] = behaviors

    rospy.Subscriber('gesture', Gesture, gesture_callback)

    rospy.loginfo("selected behavior: " + BEHAVIORS[selected])
    rate = rospy.Rate(10) 
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        select_behavior()
    except rospy.ROSInterruptException:
        pass

