/*
 *  Copyright 2016 Sasanka Nagavalli
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ros/ros.h>

#include <iostream>
#include <map>

#include <boost/foreach.hpp>

#include <boost/algorithm/clamp.hpp>
#include <boost/range/adaptor/map.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <angles/angles.h>

#include <eigen_conversions/eigen_msg.h>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include "turtlebot_behaviors/turtlebot_behaviors.h"

namespace turtlebot_behaviors {

namespace {

const double ANGULAR_VELOCITY = M_PI/4.0;

}

void turnCounterclockwise()
{
  BOOST_FOREACH(const int rid, robot_ids | boost::adaptors::map_values) {
    publishCommand(rid, 0.0, ANGULAR_VELOCITY);
  }
}

void turnClockwise()
{
  BOOST_FOREACH(const int rid, robot_ids | boost::adaptors::map_values) {
    publishCommand(rid, 0.0, -ANGULAR_VELOCITY);
  }
} 

}
