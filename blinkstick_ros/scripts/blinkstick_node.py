#!/usr/bin/env python

###############################################################################
#  Copyright 2016 Sasanka Nagavalli
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from blinkstick import blinkstick

import rospy
from std_msgs.msg import String

bstick = None

def color_callback(message):
    color = message.data
    try:
        bstick.set_color(name=color)
    except ValueError:
        rospy.errinfo('Color ' + color + ' is not a valid CSS color')


def blinkstick_node():
    global bstick
    rospy.init_node('blinkstick_node', anonymous=True)
    serial = rospy.get_param('~serial', '')
    if serial == '':
        bstick = blinkstick.find_first()
    else:
        rospy.loginfo('Trying to find BlinkStick with serial: ' + serial)
        bstick = blinkstick.find_by_serial(serial)
    if bstick is None:
        rospy.logfatal('Unable to find attached BlinkStick')
        return
    rospy.loginfo('Found BlinkStick with serial: ' + serial)
    bstick.turn_off()
    rospy.Subscriber('css_color', String, color_callback)
    rospy.on_shutdown(bstick.turn_off)
    rospy.spin()


if __name__ == '__main__':
    try:
        blinkstick_node()
    except rospy.ROSInterruptException:
        pass

